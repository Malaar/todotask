//
//  TodoTaskTests.swift
//  TodoTaskTests
//
//  Created by Malaar on 4/18/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import XCTest
@testable import TodoTask

class TaskTests: XCTestCase {
    
    var mainTask: Task!

    override func setUp() {
        super.setUp()
        mainTask = Task(title: "Main Task")
//        let task1 = Task(title: "A")
//        let task2 = Task(title: "B")
    }
    
    override func tearDown() {
        mainTask = nil
        super.tearDown()
    }
    
    
    //MARK: - modifiedDate
    
    func testModifiedDate_Title() {
        let oldModifiedDate = mainTask.modifiedDate
        
        mainTask.title = "Other Title"
        
        XCTAssertFalse(oldModifiedDate == mainTask.modifiedDate, "modifiedDate should be different after 'title' modification")
    }
    
    func testModifiedDate_Completed() {
        let oldModifiedDate = mainTask.modifiedDate
        
        mainTask.completed = true
        
        XCTAssertFalse(oldModifiedDate == mainTask.modifiedDate, "modifiedDate should be different after 'completed' modification")
    }

    func testModifiedDate_AfterAddSubtask() {
        mainTask.removeAllSubtasks()
        let oldModifiedDate = mainTask.modifiedDate

        mainTask.addSubtask(Task(title: "A"))
        
        XCTAssertFalse(oldModifiedDate == mainTask.modifiedDate, "modifiedDate should be different after adding subtask")
    }
    
    func testModifiedDate_AfterRemoveSubtask() {
        mainTask.removeAllSubtasks()
        let task = Task(title: "A")
        mainTask.addSubtask(task)
        let oldModifiedDate = mainTask.modifiedDate
        
        mainTask.removeSubtask(task)

        XCTAssertFalse(oldModifiedDate == mainTask.modifiedDate, "modifiedDate should be different after removing subtask")
    }
    
    func testModifiedDate_AfterRemoveAllSubtasks() {
        mainTask.removeAllSubtasks()
        mainTask.addSubtask(Task(title: "A"))
        mainTask.addSubtask(Task(title: "B"))
        let oldModifiedDate = mainTask.modifiedDate
        
        mainTask.removeAllSubtasks()

        XCTAssertFalse(oldModifiedDate == mainTask.modifiedDate, "modifiedDate should be different after removing all subtasks")
    }

    func testModifiedDate_AfterChangeTitleOfSubtask() {
        mainTask.removeAllSubtasks()
        let subTask = Task(title: "A")
        mainTask.addSubtask(subTask)
        let oldModifiedDate = mainTask.modifiedDate
        
        subTask.title = "Other title"

        XCTAssertFalse(oldModifiedDate == mainTask.modifiedDate, "modifiedDate should be different after changing 'title' of any subtask")
    }

    func testModifiedDate_AfterChangeCompletedOfSubtask() {
        mainTask.removeAllSubtasks()
        let subTask = Task(title: "A")
        mainTask.addSubtask(subTask)
        let oldModifiedDate = mainTask.modifiedDate
        
        subTask.completed = true
        
        XCTAssertFalse(oldModifiedDate == mainTask.modifiedDate, "modifiedDate should be different after changing 'completed' of any subtask")
    }

    func testModifiedDate_AfterChangeAnySubtaskOfSubtask() {
        mainTask.removeAllSubtasks()
        let subTask = Task(title: "A")
        mainTask.addSubtask(subTask)
        let subSubTask = Task(title: "B")
        subTask.addSubtask(subSubTask)
        let oldModifiedDate = mainTask.modifiedDate
        
        subSubTask.completed = true
        
        XCTAssertFalse(oldModifiedDate == mainTask.modifiedDate, "modifiedDate should be different after changing any subtask of subtask")
    }


    //MARK: - parentTask

    func testParentTask_AfterCreation() {
        let task = Task(title: "A")
        XCTAssertNil(task.parentTask, "parentTask should be nil after creation")
    }

    func testParentTask_OfSubtask() {
        let task = Task(title: "A")
        mainTask.addSubtask(task)
        
        XCTAssertNotNil(task.parentTask, "parentTask should be not nil for subtask")
    }

    func testParentTask_AfterRemovingSubtask() {
        let task = Task(title: "A")
        mainTask.addSubtask(task)
        
        mainTask.removeSubtask(task)
        
        XCTAssertNil(task.parentTask, "parentTask should be nil after removing subtask from parent")
    }
    
    func testParentTask_AfterRemovingItselfFromParent() {
        let task = Task(title: "A")
        mainTask.addSubtask(task)
        
        task.removeFromParentTask()
        
        XCTAssertNil(task.parentTask, "parentTask should be nil after removing itself from parent")
    }

    func testParentTask_AfterRemovingAllSubtasks() {
        let task = Task(title: "A")
        mainTask.addSubtask(task)
        mainTask.addSubtask(Task(title: "B"))

        mainTask.removeAllSubtasks()
        
        XCTAssertNil(task.parentTask, "parentTask should be nil after removing all subtasks from parent")
    }

    
    //MARK: - subtasksCount
    
    func testSubtasksCount_Zero() {
        mainTask.removeAllSubtasks()
        
        XCTAssertTrue(mainTask.subtasksCount == 0, "subtasksCount should be zero")
    }

    func testSubtasksCount_AfterAddingSubtask() {
        mainTask.removeAllSubtasks()
        
        mainTask.addSubtask(Task(title: "A"))
        
        XCTAssertTrue(mainTask.subtasksCount == 1, "subtasksCount should be one after adding single subtask")
    }

    func testSubtasksCount_AfterRemovingSubtask() {
        mainTask.removeAllSubtasks()
        let task = Task(title: "A")
        mainTask.addSubtask(task)
        
        mainTask.removeSubtask(task)
        
        XCTAssertTrue(mainTask.subtasksCount == 0, "subtasksCount should be one after removing single subtask")
    }
    
    func testSubtasksCount_AfterRemovingAllSubtasks() {
        mainTask.removeAllSubtasks()
        mainTask.addSubtask(Task(title: "A"))
        mainTask.addSubtask(Task(title: "B"))

        mainTask.removeAllSubtasks()

        XCTAssertTrue(mainTask.subtasksCount == 0, "subtasksCount should be zero after removing all subtasks")
    }
    
    
    //MARK: - hasSubtask

    func testHasSubtask_True() {
        mainTask.removeAllSubtasks()
        let task = Task(title: "A")
        mainTask.addSubtask(task)

        XCTAssertTrue(mainTask.hasSubtask(task), "Task should have subtask after adding")
    }

    func testHasSubtask_False() {
        mainTask.removeAllSubtasks()
        let task = Task(title: "A")
        
        XCTAssertFalse(mainTask.hasSubtask(task), "Task should have subtask after adding")
    }

    func testHasSubtask_FalseAfterRemovingSubtask() {
        mainTask.removeAllSubtasks()
        let task = Task(title: "A")
        mainTask.addSubtask(task)
        mainTask.removeSubtask(task)
        
        XCTAssertFalse(mainTask.hasSubtask(task), "Task should haven't subtask after removing it")
    }

    func testHasSubtask_FalseAfterRemovingAllSubtasks() {
        mainTask.removeAllSubtasks()
        let task = Task(title: "A")
        mainTask.addSubtask(task)
        mainTask.removeAllSubtasks()
        
        XCTAssertFalse(mainTask.hasSubtask(task), "Task should haven't subtask after removing all subtasks")
    }
    
    
    //MARK: - addSubtask

    func testAddSubtask_True() {
        mainTask.removeAllSubtasks()
        let task = Task(title: "A")
        mainTask.addSubtask(task)
        
        XCTAssertTrue(mainTask.hasSubtask(task), "Task should have subtask after adding")
    }

    func testAddSubtask_CantDublicateSubtasks() {
        mainTask.removeAllSubtasks()
        let task = Task(title: "A")
        mainTask.addSubtask(task)
        mainTask.addSubtask(task)

        XCTAssertTrue(mainTask.subtasksCount == 1, "Can't dublicate subtasks")
    }
    

    //MARK: - removeSubtask

    func testRemoveSubtask_RemovingOwnSubtask() {
        mainTask.removeAllSubtasks()
        let task = Task(title: "A")
        mainTask.addSubtask(task)
        
        mainTask.removeSubtask(task)
        
        XCTAssertTrue(mainTask.subtasksCount == 0, "Should remove single subtask")
    }
    
    func testRemoveSubtask_RemovingNotOwnSubtask() {
        mainTask.removeAllSubtasks()
        let task = Task(title: "A")
        mainTask.addSubtask(task)
        
        mainTask.removeSubtask(Task(title: "B"))
        
        XCTAssertTrue(mainTask.subtasksCount == 1, "Nothing changed if try to remove not own subtask")
    }

    //MARK: - removeAllSubtasks
    
    func testRemoveAllSubtasks_ForTaskWithoutSubtasks() {
        let task = Task(title: "Empty task")
        
        task.removeAllSubtasks()
        
        XCTAssertTrue(mainTask.subtasksCount == 0, "Nothing changed if try to remove all subtasks of empty task")
    }

    func testRemoveAllSubtasks_ForTaskWithSubtasks() {
        mainTask.removeAllSubtasks()
        mainTask.addSubtask(Task(title: "A"))
        mainTask.addSubtask(Task(title: "B"))
        
        mainTask.removeAllSubtasks()
        
        XCTAssertTrue(mainTask.subtasksCount == 0, "List of subtasks should be empty after removeing all subtasks")
    }
    
    
    //MARK: - removeFromParentTask
    
    func testRemoveFromParentTask() {
        mainTask.removeAllSubtasks()
        let task = Task(title: "A")
        mainTask.addSubtask(task)
        
        task.removeFromParentTask()
        
        let toCheck = task.parentTask == nil && mainTask.hasSubtask(task) == false
        
        XCTAssertTrue(toCheck, "Should be successfully removed from parent")
    }


    //MARK: - isAllSubtasksCompleted

    func testIsAllSubtasksCompleted_True() {
        mainTask.removeAllSubtasks()
        let task1 = Task(title: "A")
        task1.completed = true
        mainTask.addSubtask(task1)
        let task2 = Task(title: "B")
        task2.completed = true
        mainTask.addSubtask(task2)
        
        let toCheck = mainTask.isAllSubtasksCompleted()
        
        XCTAssertTrue(toCheck, "All subtasks should be completed")
    }

    func testIsAllSubtasksCompleted_False() {
        mainTask.removeAllSubtasks()
        let task1 = Task(title: "A")
        task1.completed = true
        mainTask.addSubtask(task1)
        let task2 = Task(title: "B")
        task1.completed = false
        mainTask.addSubtask(task2)
        
        let toCheck = mainTask.isAllSubtasksCompleted()
        
        XCTAssertFalse(toCheck, "Not all subtasks are completed")
    }
    
    //MARK: - completeAllSubtasks
    
    func testCompleteAllSubtasks_True() {
        mainTask.removeAllSubtasks()
        let task1 = Task(title: "A")
        mainTask.addSubtask(task1)
        let task2 = Task(title: "B")
        mainTask.addSubtask(task2)

        mainTask.completeAllSubtasks()
        
        let toCheck = task1.completed && task2.completed
        
        XCTAssertTrue(toCheck, "All subtasks should be completed")
    }

    //MARK: - Equation
    
    func testEquation_ForTaskType_False() {
        let task1 = Task(title: "A")
        let task2 = Task(title: "B")
        
        XCTAssertFalse(task1 == task2, "Tasks shouldn't be equal")
    }

    func testEquation_ForTaskType_True() {
        let task1 = Task(title: "A")
        let task2 = task1
        
        XCTAssertTrue(task1 == task2, "Tasks should be equal")
    }

    func testEquation_ForTaskableType_False() {
        let task1: Taskable = Task(title: "A")
        let task2: Taskable = Task(title: "B")
        
        XCTAssertFalse(task1.asEquatable() == task2.asEquatable(), "Taskables shouldn't be equal")
    }
    
    func testEquation_ForTaskableType_True() {
        let task1: Taskable = Task(title: "A")
        let task2: Taskable = task1
        
        XCTAssertTrue(task1.asEquatable() == task2.asEquatable(), "Taskables should be equal")
    }
    
    func testTaskAsHashable () {
        let task: Taskable = Task(title: "A")
        var dictionary: [AnyHashableTask: String] = [:]
        
        let testValue = "something"
        dictionary[task.asHashable()] = testValue
        
        XCTAssert(dictionary[task.asHashable()] == testValue, "Taskable object can be used as key for dictionary")
    }
}
