//
//  Task.swift
//  TodoTask
//
//  Created by Malaar on 4/17/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


//MARK: - Task

open class Task: Taskable {
        
    open var title: String {
        didSet {
            updateModifiedDate()
        }
    }
    
    open var completed: Bool {
        didSet {
            updateModifiedDate()
        }
    }
    
    open private(set) var modifiedDate: Date
    
    open private(set) weak var parentTask: Taskable?
    
    private var subtasks = [Taskable]()

    
    //MARK: - Initialization

    public required init(title: String, completed: Bool = false, modifiedDate: Date? = nil) {
        self.title = title
        self.completed = completed
        self.modifiedDate = modifiedDate ?? Date()
    }
    
    
    //MARK: - Subtasks management
    
    open var subtasksCount: Int {
        return subtasks.count
    }

    open func hasSubtask(_ task: Taskable) -> Bool {
        for subtask in subtasks {
            if subtask.asEquatable() == task.asEquatable() {
                return true
            }
        }
        return false
    }
    
    open func addSubtask(_ task: Taskable) {
        if hasSubtask(task) { return }
        task.removeFromParentTask()
        subtasks.append(task)
        task.setParentTask(self)
        updateModifiedDate()
    }
    
    open func removeSubtask(_ task: Taskable) {
        guard let index = indexOfSubtask(task) else { return }
        subtasks.remove(at: index)
        task.setParentTask(nil)
        updateModifiedDate()
    }
    
    open func removeAllSubtasks() {
        subtasks.forEach { $0.setParentTask(nil) }
        subtasks.removeAll()
        updateModifiedDate()
    }
    
    open func removeFromParentTask() {
        parentTask?.removeSubtask(self)
        updateModifiedDate()
    }

    
    //MARK: - Subtask completion
    
    open func isAllSubtasksCompleted() -> Bool {
        for task in subtasks {
            if !task.completed {
                return false
            }
        }
        return true
    }
    
    open func completeAllSubtasks() {
        for task in subtasks {
            task.completed = true
            //TODO: Should all subtasks also be completed?
//            task.completeAllSubtasks()
        }
    }
    
    open func isEqual(to other: Taskable) -> Bool {     // to support equation in case of inharitance of Task
        guard type(of: other) == Task.self, let otherTask = other as? Task else { return false }
        return self == otherTask
    }
}


//MARK: - Subtask by index

extension Task {
    
    open func subtask(at index: Int) -> Taskable? {
        return index >= 0 && index < subtasks.count ? subtasks[index] : nil
    }
    
    open func indexOfSubtask(_ task: Taskable) -> Int? {
        return subtasks.index { $0.asEquatable() == task.asEquatable() }
    }
}


//MARK: - Equatable protocol

extension Task: Equatable {
    
    open static func == (lhs: Task, rhs: Task) -> Bool {
        if rhs.subtasksCount != lhs.subtasksCount { return false }
        if lhs.title != rhs.title || lhs.completed != rhs.completed || lhs.modifiedDate != rhs.modifiedDate { return false }
        for i in 0..<rhs.subtasksCount {
            if lhs.subtask(at: i)?.asEquatable() != rhs.subtask(at: i)?.asEquatable() {
                return false
            }
        }
        return true
    }
}


//MARK: - Hashable protocol

extension Task: Hashable {
    
    open var hashValue: Int {
        return title.hashValue ^ completed.hashValue ^ modifiedDate.hashValue ^ subtasksCount.hashValue
    }
}


//MARK: - Internal

extension Task {
    
    public func setParentTask(_ task: Taskable?) {
        self.parentTask = task
    }

    public func updateModifiedDate() {
        self.modifiedDate = Date()
//        TODO: should we change modifiedDate for every parent of this task?
        var parent = self.parentTask
        while parent != nil {
            parent?.updateModifiedDate()
            parent = parent?.parentTask
        }
    }
}
