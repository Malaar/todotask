//
//  AnyEquatabeTask.swift
//  TodoTask
//
//  Created by Malaar on 4/17/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


//MARK: - AnyEquatabeTask

public class AnyEquatabeTask: Taskable {
    
    internal let taskable: Taskable

    public init(_ taskable: Taskable) {
        self.taskable = taskable
    }
    
    public var title: String {
        get { return taskable.title }
        set { taskable.title = newValue }
    }
    
    public var modifiedDate: Date {
        return taskable.modifiedDate
    }
    
    public var completed: Bool {
        get { return taskable.completed }
        set { taskable.completed = newValue }
    }
    
    public var parentTask: Taskable? {
        return taskable.parentTask
    }
    
    public var subtasksCount: Int {
        return taskable.subtasksCount
    }
    
    public func hasSubtask(_ task: Taskable) -> Bool {
        return taskable.hasSubtask(task)
    }
    
    public func addSubtask(_ task: Taskable) {
        taskable.addSubtask(taskable)
    }
    
    public func removeSubtask(_ task: Taskable) {
        taskable.removeSubtask(task)
    }
    
    public func removeAllSubtasks() {
        taskable.removeAllSubtasks()
    }
    
    public func removeFromParentTask() {
        taskable.removeFromParentTask()
    }
    
    public func isAllSubtasksCompleted() -> Bool {
        return taskable.isAllSubtasksCompleted()
    }
    
    public func completeAllSubtasks() {
        taskable.completeAllSubtasks()
    }
    
    public func setParentTask(_ task: Taskable?) {
        taskable.setParentTask(task)
    }

    public func updateModifiedDate() {
        taskable.updateModifiedDate()
    }
    
    public func asHashable() -> AnyHashableTask {
        return taskable.asHashable()
    }
    
    public func getHashValue() -> Int {
        return taskable.getHashValue()
    }
}


//MARK: - Equatable protocol

extension AnyEquatabeTask: Equatable {
    
    public static func == (lhs: AnyEquatabeTask, rhs: AnyEquatabeTask) -> Bool {
        return lhs.taskable.isEqual(to: rhs.taskable)
    }
}
