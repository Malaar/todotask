//
//  Taskable.swift
//  TodoTask
//
//  Created by Malaar on 4/17/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


//MARK: - Taskable

public protocol Taskable: class, CustomStringConvertible {
    
    var title: String { get set }
    var modifiedDate: Date { get }
    var completed: Bool { get set }
    
    var parentTask: Taskable? { get }
    var subtasksCount: Int { get }
    
    func hasSubtask(_ task: Taskable) -> Bool
    
    func addSubtask(_ task: Taskable)
    func removeSubtask(_ task: Taskable)
    func removeAllSubtasks()
    func removeFromParentTask()

    func isAllSubtasksCompleted() -> Bool
    func completeAllSubtasks()
    
    func asEquatable() -> AnyEquatabeTask
    func isEqual(to task: Taskable) -> Bool
    
    func asHashable() -> AnyHashableTask
    func getHashValue() -> Int

    /**
     * For internal purposes ony. You don't need to use it directly in your code, in other case behaviour is undefined
     **/
    func setParentTask(_ task: Taskable?)

    /**
     * For internal purposes ony. You don't need to use it directly in your code, in other case behaviour is undefined
     **/
    func updateModifiedDate()
}


//MARK: - CustomStringConvertible protocol

public extension Taskable {
    
    public var description: String {
        return """
        title: \(title),
        completed: \(completed),
        modifiedDate: \(modifiedDate),
        subtasksCount: \(subtasksCount))
        """
    }
}


//MARK: - Equatable protocol

public extension Taskable where Self: Equatable {
    
    public func asEquatable() -> AnyEquatabeTask {
        return AnyEquatabeTask(self)
    }

    public func isEqual(to other: Taskable) -> Bool {
        guard let other = other as? Self else { return false }
        return self == other
    }
}


//MARK: - Hashable protocol

public extension Taskable where Self: Hashable {
    
    public func asHashable() -> AnyHashableTask {
        return AnyHashableTask(self)
    }
    
    public func getHashValue() -> Int {
        return self.hashValue
    }
}
