//
//  AnyHashableTask.swift
//  TodoTask
//
//  Created by Malaar on 4/18/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation

public class AnyHashableTask: AnyEquatabeTask, Hashable {
    
    public var hashValue: Int {
        return taskable.getHashValue()
    }
}
