# TodoTask

TodoTask is a simple test static library, which implements model of simple todo task.
Project was created with paradigm of Protocol Oriented Programming.
To achieve Equatable and Hashable behavior of todo Task via protocol, concept of `Type Erasure` was used

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for overview and testing purposes.

### Prerequisites

* Cocoapods [requirements](https://cocoapods.org/)
* Mac OS X 10.13+, Xcode 9.3+

### Installing

1. Clone TodoTask repository

			git clone git@bitbucket.org:Malaar/todotask.git

2. Done.

## Running

To run on iOS/mac

Open TodoTask.xcodeproj in Xcode

Enjoy.

## Logic details

Protocols:

* `Taskable` - protocol, provide interface for any todo task


Classes:

* `AnyEquatableTask` - Type Erasure to add Equatable behavior for any object (accesed via `Taskable` protocol) with adopted `Taskable` and Equatable protocols
* `AnyHashableTask` - Type Erasure to add Hashable behavior for any object (accesed via `Taskable` protocol) with adopted `Taskable` and Hashable protocols
* `Task` - object adopted `Taskable`, Equatable and Hashable protocols

## Unit Tests
* to test logic of Task, behavior of Hashable and Equatable protocols (via Testable protocol) `TaskTests` was used



## Author

* **[Andrew Korshilovskiy](http://www.linkedin.com/in/korshilovskiy)** - development

## License

(c) All rights reserved.
This project is licensed under the **Proprietary Software License** - see the [LICENSE](http://www.binpress.com/license/view/l/358023be402acff778a934083b76b86f) url for details
